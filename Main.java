import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner= new Scanner(System.in);
        ListReader listReader = new ListReader();
        ArrayList<String> to = new ArrayList<>();

        System.out.println("podaj ścieżkę do pliku :");
        String path = scanner.nextLine();
        to = listReader.getMailAddresses(path);
        System.out.println("podaj login :");
        String from = scanner.nextLine();
        System.out.println("podaj hasło :");
        String pass = scanner.nextLine();
        System.out.println("podaj tytuł :");
        String subject = scanner.nextLine();
        System.out.println("podaj treść :");
        String body = scanner.nextLine();

        sendFromGmail(from,pass,to,subject,body);

    }
    private static void sendFromGmail(String from, String pass,ArrayList<String> to,String subject, String body){

        java.util.Properties properties =new java.util.Properties();

        String host = "smtp.gmail.com";
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.password", pass);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(properties,null);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress [] toAddress= new InternetAddress[to.size()];
            for (int i=0 ;i<to.size();i++){
                toAddress[i]=new InternetAddress(to.get(i));
            }
            for (int i=0;i<toAddress.length;i++){
                message.addRecipient(Message.RecipientType.TO,toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }

}
