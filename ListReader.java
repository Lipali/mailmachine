import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ListReader {

    public ArrayList<String> getMailAddresses (String path) {
        ArrayList <String> addresses = new ArrayList<>();
        Path filePath = Paths.get(path);

             try{
                 addresses = (ArrayList) Files.readAllLines(filePath);
             }catch (IOException e){
                System.out.println("brak pliku");
             }
        return addresses;
    }

}
